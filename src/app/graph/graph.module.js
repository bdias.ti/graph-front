angular.module('app.graph', [
  'ui.router',
  'graph.controllers.GraphCtrl',
  'graph.services.GraphService'
])

.config(function config($stateProvider) {
  $stateProvider.state('graph', {
    url: '/graph',
    templateUrl: 'graph/graph.view.html'
  });
});

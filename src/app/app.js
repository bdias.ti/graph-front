angular.module('angular-material-boilerplate', [ 
  'ngAnimate',
  'ngAria',
  'ngMaterial',
  'ui.router',
  'nvd3',
  'templates',
  'app.shared',
  'app.layout',
  'app.toolbar',
  'app.sidenav',
  'app.graph'
])

.config(function($mdThemingProvider) {
  $mdThemingProvider.theme('default')
    .primaryPalette('indigo')
    .accentPalette('purple', {
      'default': '200'
    });
}).constant('BASE_URL', 'http://localhost:8080/graph')

.run(['$state', function ($state) {
  $state.go('graph');
}]);

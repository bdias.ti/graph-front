
angular.module('sidenav.services.SideNav', [])
  .factory('SideNav', function() {

    var menuItems = [
      {
        name: 'Graph',
        icon: '',
        route: 'graph'
      }
    ];

    var service = {
      getMenuItems: menuItems
    };
    return service;

  });

angular.module('graph.controllers.GraphCtrl', [])
.controller('GraphCtrl', function($scope,GraphService,$mdDialog,$mdToast) {
    $scope.lerGraph = function(id){
        GraphService.lerGraph(id).then(function successCallback(response){
            var response = response.data; 
            console.log(response.data);
            $scope.data = response.data.data;
            var alfabeto =  ["A","B","C","D","E","F","G","H","I","J","L","M","N","O","P","Q","R","S","T","U","V","X","Z"];
            var nodesEncontrados = [];
            //Declarar array de links
            var links =[];
            var nodes = [];
            for(var element in response.data){
                console.log(element)
               var source = alfabeto.indexOf(response.data[element].source);
               var target = alfabeto.indexOf(response.data[element].target);
               nodesEncontrados.push(response.data[element].source);
               nodesEncontrados.push(response.data[element].target);
                var element = {
                    "source": source,
                    "target": target,
                    "value": response.data[element].distance 
                } 
                links.push(element);
            }
            var uniq = nodesEncontrados.reduce(function(a,b){
                if (a.indexOf(b) < 0 ) a.push(b);
                return a;
              },[]);
              console.log(uniq);
              for (var element in uniq){
                var node = {
                    "name": uniq[element],
                    "group:": 1
                }
                nodes.push(node);
              }


            $scope.data = {
                "nodes":nodes,
                "links":links
            }

        },function errorCallback(response){ 
            $scope.showToast("Não conseguiu ler o graph especificado");
        }); 
    }
    
    //Lib d3 responsável por renderizar o graph na página
    var color = d3.scale.category20()
    $scope.options = {
        chart: {
            type: 'forceDirectedGraph',
            height: 350,
            width:1035,
            margin:{top: 20, right: 20, bottom: 20, left: 20},
            color: function(d){
                return color(d.group)
            },
            nodeExtras: function(node) {
                node && node
                .append("text")
                .attr("dx", 8)
                .attr("dy", ".35em")
                .text(function(d) { return d.name })
                .style('font-size', '10px');
            }
        }
    };
     });
    
    
    
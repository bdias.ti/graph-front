angular.module('graph.services.GraphService', [])
.factory('GraphService', function(BASE_URL, $http) {
  
  var factory = {};

  factory.lerGraph = function(id){ 
    return $http.get(`${BASE_URL}/${id}`);
  }
  
  return factory;
});
